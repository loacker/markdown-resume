---
title: Matteo Piccinini
---

***
![](photo.jpg){width=1in}

+XX XXX XXXXXXX 

matteo.piccinini@XXXXX.XXX

[www.linkedin.com/in/piccininimatteo](https://www.linkedin.com/in/piccininimatteo)

Via X. XXXXXXX, XX - XXXXX - XXXXXXXXXXX (XX) - Italy

\

Dear Hiring Manager,

\

I am writing to express my interest in the position XXXXXXXXXXXXXX; In order to establish a fruitful collaboration with your company I have enclosed a copy of my resume for your review.

I am currently coming back from a long sabbatical; I was forced to step away from work by a sudden negative event, personal health and later the pandemic brought me to the conclusion that I needed much more than an extended vacation, this time off provided me with the psychological safe space I needed and to think about my priorities in life; I moved back to Italy at the beginning of 2023 from The Netherlands where I lived for the last eight years to spend some time with my family; I am ready to take on new career goals now that I am fully recovered and I am available to start immediately and to relocate worldwide.

I have over fifteen years of professional experience and I am a very early adopter of GNU/Linux, I was employed by some important companies as a Cloud Solution Architect and OpenStack Technical specialist for Europe, Middle East and Africa where I had the opportunity to work with customers all around the world, supporting the adoption of free and open source software and cloud technologies such as Red hat Enterprise Linux, OpenStack, OpenShift, Kubernetes, Puppet and Ansible.

My area of knowledge ranging from the architecture design, supporting front line sales teams operations matching requirements to actual customer needs, to the Q/A and to production delivery stages interfacing with delivery managers, product managers and support engineers.
  
I am a passionate geek, free and open source software evangelist, python enthusiast, I strongly believe in DevOps practice and agile development; I spend most of my free time studying, attending conferences around Europe and actively participating in meetup events, places where I can continuously enhance and practice my public speaking skills.

I strongly believe I possess the right combination of skills, passion and experience you are looking for and I am confident that these make me an ideal candidate for this position, I refer to my resume for the details about my experiences and my technical skills hoping it can be to your liking.

![](signature.jpg){width=1in}

\

Kind regards,  
Matteo Piccinini

