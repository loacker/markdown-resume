---
title: Matteo Piccinini
---

***
![](photo.jpg){width=1in}

+XX XXX XXXXXXX 

matteo.piccinini@XXXXX.XXX

[www.linkedin.com/in/piccininimatteo](https://www.linkedin.com/in/piccininimatteo)

Via X. XXXXXXX, XX - XXXXX - XXXXXXXXXXX (XX) - Italy

\

About me
--------

***

**F**ree and **O**pen **S**ource **S**oftware enthusiast with a strong background in **GNU/Linux**, Cloud Computing
and Automation.

Curious, motivated, flexible and accountable. Focused and committed to excellence.


Education
---------

***

2017
:    **Red Hat Certified Engineer in Red Hat OpenStack** - ID: 120-151-634.

2015
:    **Red Hat Certified System Administrator in Red Hat OpenStack** - ID: 120-151-634.

2014
:    Self study python programming language. 

2013
:    Intel Enterprise Edition for Lustre (IEEL) training, CSCS, Lugano.

2012
:    **Red Hat Certified Engineer (RHCE)** - ID: 120-151-634. 

2012
:    **Red Hat Certified System Administrator (RHCSA)** - ID: 120-151-634.

2005
:    Internship in the IT Security department, Telecom Italia, Roma.

2004
:    Internship in Security Platform department, TI Sparkle, Roma.

2003/2005
:    Associate Degree in Information Technology, Centro ELIS, Roma.

1997/2003
:    High School Diploma in Telecommunication, I.T.I.S., Avezzano.


Skills
------

***

**Operating System**: RHEL, Ubuntu, Archlinux, Solaris.

**Containers and Virtualization**: OpenStack, K8s, OpenShift, Podman, Docker.

**Automation and Configuration Management**: Ansible, Salt, Puppet.

**Programming Languages**: Python, Bash.

**Networking**: Cisco, Tungsten Fabric, OVN, Qlogic. 

**Storage**: Ceph, Gluster, FC, FCoE, iSCSI.

**Database**: Oracle, Postgres, Mariadb, MongoDB. 

\

Languages
-------------

***

**Italian**: Mother tongue  

**English**: Reading: C1 - Writing: B1 - Speaking : B2 - Listening: B2 


Work experience
---------------

***

2019/2020
:    **Mirantis** - EMEA Remote / Amsterdam - **Cloud Solution Architect**.
:    Highlights: OpenStack, Kubernetes, Ceph, Ubuntu, NFV.

2016/2019
:    **Red Hat** - EMEA Remote / Amsterdam - **Specialist Solution Architect**.
:    Highlights: OpenStack, OpenShift, Ceph, RHEL, NFV.

2015/2016
:    **Bright Computing** - Amsterdam - **Support and Deployment Engineer**.
:    Highlights: HPC, OpenStack, Kubernetes, Ceph.

2014/2015
:    **Banca D'Italia** - Roma -  **Consulting for JNET2000** - OSIRIDE2 project.
:    Highlights: HPC, Red Hat Cluster Suite, Lustre, Openlava, Puppet.

2013/2014
:    **Red Hat** - Roma - **Consulting for JNET2000**.
:    Highlights: RHEL, Puppet, Satellite, Openshift.

2013/2014
:    **UN Word Food Programme** - Roma - **Consulting for JNET2000** .
:    Highlights: LXC, Libvirt, Selinux, Bash.

2013/2014
:    **Banca D'Italia** - Roma - **Consulting for JNET2000** - Webfarm2 project.
:    Highlights: RHEL, RHEV, Puppet, Satellite, Cloudforms, Foreman.

2012/2013
:    **ASI Science Data Center** - Frascati - **Consulting for Sistematica**.
:    Highlights: RHEL, XEN, Open Nebula.

2009/2012
:    **Leonardo - Selex SeMa** - Roma - **Consulting for Sistematica**. 
:    Highlights: Oracle RAC, Oracle AS, RHEL, Red Hat Cluster Suite.

2008/2009
:    **Abruzzo Engineering** - L'Aquila - **Consulting for Sistematica**. 
:    Highlights: Oracle RAC, Oracle AS, RHEL, Red Hat Cluster Suite.

2007/2008
:    **Societa' Italiana Condotte d'acqua** - Napoli - **Consulting for Orienta**. 
:    Highlights: Quality control.

2005/2006
:    **Ericsson** - Milano / Roma - **Consulting for Teleca**.
:    Highlights: Solaris, Siemens BTS, Alcatel SDH, Ericsson PDH.

\
\

Referees are available on request.

I'm available to start immediately and to relocate worldwide.

\
![](signature.jpg){width=1in}
\

Capistrello, August 2023

Matteo Piccinini

