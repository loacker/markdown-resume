Markdown PDF resume
===================

PDF resume and cover letter from markdown.

Requirements:
-------------

``
pandoc-cli 
python-weasyprint 
make 
``

On archlinux:
-------------

```
sudo pacman -S pandoc-cli python-weasyprint make
```

Update the cover.md, resume.md, photo.jpg and signature.jpg and create the PDF files:
-------------------------------------------------------------------------------------

```
make
```

Additional commands to update respectively only letter or resume:
-----------------------------------------------------------------

```
make letter-pdf
make resume-pdf
```

For an HTML version of the resume or letter use:
------------------------------------------------

```
make html
make letter-html
make resume-html
```

See the PDF files in the repo to have an idea of the output.
------------------------------------------------------------
