RESUME_FILENAME=matteo-piccinini-resume
LETTER_FILENAME=matteo-piccinini-letter

all: pdf

pdf: letter-pdf resume-pdf

html: letter-html resume-html

letter-pdf:
	pandoc  letter.md \
		--css style.css \
		--standalone \
	        --pdf-engine=weasyprint	\
		--output ${LETTER_FILENAME}.pdf

letter-html:
	pandoc  letter.md \
		--css style.css \
		--standalone \
	        --to html \
		--output ${LETTER_FILENAME}.html

resume-pdf:
	pandoc  resume.md \
		--css style.css \
		--standalone \
	        --pdf-engine=weasyprint	\
		--output ${RESUME_FILENAME}.pdf

resume-html:
	pandoc  resume.md \
		--css style.css \
		--standalone \
	        --to html \
		--output ${RESUME_FILENAME}.html


#  vim: set ts=8 sw=0 sts=0 tw=0 ff=unix ft=make et ai :
